#!/usr/bin/env bash

set -euo pipefail

kubectl apply -f namespace.yml

# First delete evertying we may have created before
kubectl delete deployments --all --namespace demo
kubectl delete services --all --namespace demo

sleep 10

# Next deploy services. These must be created before the pods so pods
# populate the service.
kubectl create -f data-tier-service.yml --namespace demo
kubectl create -f app-tier-service.yml --namespace demo

# Create pods
kubectl create -f data-tier-deployment.yml --namespace demo
kubectl create -f app-tier-deployment.yml --namespace demo
kubectl create -f support-tier-deployment.yml --namespace demo
